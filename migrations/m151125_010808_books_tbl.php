<?php

use yii\db\Schema;
use yii\db\Migration;

class m151125_010808_books_tbl extends Migration
{

    private $_tableName = '{{%books}}';

    public function up()
    {
        //Attention! we assume that as bd used MySQL
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $relates = ['authors'=>'{{%authors}}'];

        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'date_created' => $this->date()->notNull(),
            'date_update' => $this->date()->notNull(),
            'date' => $this->date()->notNull(),
            'preview' => $this->string(255)->notNull(),
            'author_id' => $this->integer()->notNull()
        ], $tableOptions);

        $this->createIndex('books_authors',$this->_tableName,'author_id');
        $this->addForeignKey('fk_books_authors', $this->_tableName, 'author_id', $relates['authors'], 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        echo "m151125_010808_books_tbl cannot be reverted.\n";
        $this->dropTable($this->_tableName);
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
