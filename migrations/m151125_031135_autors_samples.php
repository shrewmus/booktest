<?php

use yii\db\Schema;
use yii\db\Migration;

class m151125_031135_autors_samples extends Migration
{
    private $_tableName = '{{%authors}}';
    public function up()
    {
        $this->batchInsert($this->_tableName,['firstname','lastname'],[
            ['Михаил','Слонимский'],
            ['Сергей','Голубов'],
            ['Андрей','Платонов'],
            ['Роджер','Желязны'],
            ['Уильям','Гибсон']
            ]);
    }

    public function down()
    {
        echo "m151125_031135_autors_samples cannot be reverted.\n";
        //no down
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
