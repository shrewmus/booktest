<?php

use yii\db\Migration;

class m151125_002214_authors_tbl extends Migration
{

    private $_tableName = '{{%authors}}';

    public function up()
    {
        //Attention! we assume that as bd used MySQL
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->notNull(),
            'lastname' => $this->string()
        ], $tableOptions);
    }

    public function down()
    {
        echo "m151125_002214_authors_tbl cannot be reverted.\n";
        $this->dropTable($this->_tableName);
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
