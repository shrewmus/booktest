<?php
/**
 * @file books_tbl_row.php
 * @author shrewmus (shrewmus@yandex.ru contact@shrewmus.name)
 * Date: 11/25/15
 * Time: 5:24 AM
 * (c) 2015
 */

/** @var $this \yii\web\View */
/** @var $book \app\models\Books */
?>
<tr>
    <td><?= $book->id ?></td>
    <td><?= $book->name ?></td>
    <td><img src="<?= $book->preview ?>" class="pic"><img src="<?= $book->preview ?>" class="picbig"></td>
    <td><?= $book->author->getFullName() ?></td>
    <td><?= $book->date ?></td>
    <td><?= $book->date_update ?></td>
    <td><a href="/books/edit/<?= $book->id ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
    <td><a href="#" data-book="<?= $book->id ?>" class="view-btn"><span class="glyphicon glyphicon-zoom-in"></span></a></td>
    <td><a href="#" data-book="<?= $book->id ?>" class="delete-btn"><span class="glyphicon glyphicon-remove"></span></a></td>
</tr>
