<?php
/**
 * @file books_form.php
 * @author shrewmus (shrewmus@yandex.ru contact@shrewmus.name)
 * Date: 11/25/15
 * Time: 5:10 AM
 * (c) 2015
 */

/** @var $this \yii\web\View */
/** @var $book \app\models\Books */

?>
<form class="form-horizontal">
    <div class="form-group">
        <label for="name" class="col-sm-4">Название</label>
        <div class="col-sm-8">
            <p><?= $book->name ?></p>
        </div>
    </div>
    <div class="form-group">
        <label for="author" class="col-sm-4">автор</label>
        <div class="col-sm-8">
            <?= $book->author->getFullName() ?>
        </div>
    </div>
    <div class="form-group row">
        <label for="preview" class="col-sm-4">Превью</label>

        <div class="col-sm-8">
            <img class="img-thumbnail" id="imgPreview" <?php if($book->preview){ echo 'src="'.$book->preview.'"'; } ?>>
        </div>
    </div>
    <div class="form-group">
        <label for="date" class="col-sm-4">Дата выхода книги</label>
        <div class="col-sm-8">
            <p><?= $book->date ?></p>
        </div>
    </div>

</form>
