<?php
/**
 * @file index.php
 * @author shrewmus (shrewmus@yandex.ru contact@shrewmus.name)
 * Date: 11/25/15
 * Time: 4:29 AM
 * (c) 2015
 */

/** @var $this \yii\web\View */
/** @var $books \app\models\Books[] */
/** @var $authors \app\models\Authors[] */
/** @var $heads array */
/** @var $search array */

$isSearchRepost = isset($search);

?>
<div class="row">
    <div class="col-sm-10">
        <form class="form-horizontal" action="/books/search" id="frmSearch" method="post">
            <div class="form-group">
                <div class="col-sm-4">
                    <select name="author" id="author" class="form-control" data-placeholder="автор">
                        <option value=""></option>
                        <?php
                        foreach ($authors as $author) { ?>
                            <option
                                value="<?= $author->id ?>" <?php
                            if($isSearchRepost){
                                if($search['author'] && $search['author'] == $author->id){
                                    echo 'selected="selected"';
                                }
                            }?>><?= $author->getFullName() ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
                <div class="col-sm-4">
                    <input type="text" id="name" name="name" placeholder="Название книги" class="form-control" value="<?php
                    if($isSearchRepost){
                        echo $search['name'];
                    }
                    ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <input type="text" id="date_from" class="calendar form-control" name="date_from" value="<?php
                    if($isSearchRepost){
                        echo $search['date_from'];
                    }
                    ?>">
                </div>
                <div class="col-sm-4">
                    <input type="text" id="date_to" class="calendar form-control" name="date_to" value="<?php
                    if($isSearchRepost){
                        echo $search['date_to'];
                    }
                    ?>">
                </div>
            </div>

        </form>
    </div>
    <div class="col-sm-1">
    <?php if(! Yii::$app->user->isGuest){ ?>
    <a href="/books/create" class="btn btn-default">Добавить</a>
    <?php } ?>
    </div>
    <div class="col-sm-1">
        <a href="#" class="btn bg-primary" id="btnSearch">Искать</a>
    </div>
</div>
<div>
    <table class="table table-striped tablesorter">
        <thead>
        <?php
        foreach ($heads as $head) {?>
            <th><?= $head ?></th>
        <?php } ?>
            <th colspan="3">Действия</th>
        </thead>
        <tbody>
        <?php
        foreach($books as $book){
            echo $this->render('@app/views/books/books_tbl_row.php',['book'=>$book]);
        }
        ?>
        </tbody>
    </table>
</div>
<div class="modal fade bs-example-modal-lg" id="bookViewModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
