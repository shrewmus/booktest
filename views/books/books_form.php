<?php
/**
 * @file books_form.php
 * @author shrewmus (shrewmus@yandex.ru contact@shrewmus.name)
 * Date: 11/25/15
 * Time: 5:10 AM
 * (c) 2015
 */

/** @var $this \yii\web\View */
/** @var $book \app\models\Books */
/** @var $authors \app\models\Authors[]*/
/** @var $type string */

$ids = ['create'=>0,'edit'=>$book->id];
$pageHead = ($type === 'create')?'Создание книги':'Редактирование книги';

?>
<div class="page-header"><h2><?= $pageHead ?></h2></div>
<form class="form-horizontal" method="post" action="/books/save" enctype="multipart/form-data">
    <input type="hidden" name="book_id" value="<?= $ids[$type] ?>">
    <input type="hidden" name="type" value="<?= $type ?>">
    <div class="form-group">
        <label for="name" class="col-sm-4">Название</label>
        <div class="col-sm-4">
            <input type="text" name="name" id="name" placeholder="Название" value="<?= $book->name ?>" maxlength="255" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label for="author" class="col-sm-4">автор</label>
        <div class="col-sm-4">
            <select name="author" id="author" class="form-control">
                <option value=""></option>
                <?php
                foreach($authors as $author){ ?>
                    <option value="<?= $author->id ?>"
                        <?php if ($book->author) {
                            if($book->author->id == $author->id){
                                echo 'selected="selected"';
                            }
                    } ?>>
                        <?= $author->getFullName() ?></option>
                <?php }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="preview" class="col-sm-4">Превью</label>
        <div class="col-sm-4">
            <input type="file" id="preview" name="preview" class="form-control">
        </div>
        <div class="col-sm-4">
            <img class="img-thumbnail" id="imgPreview" <?php if($book->preview){ echo 'src="'.$book->preview.'"'; } ?>>
        </div>
    </div>
    <div class="form-group">
        <label for="date" class="col-sm-4">Дата выхода книги</label>
        <div class="col-sm-4">
            <input type="text" value="<?= $book->date ?>" id="date" name="date" class="calendar form-control">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-8 col-sm-offset-2">
            <a href="/" class="btn btn-default">Отмена</a>
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </div>
</form>
