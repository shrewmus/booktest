/**
 * @file common.js
 * @package
 * @author shrewmus (shrewmus@yandex.ru contact@shrewmus.name)
 * Date: 11/25/15
 * Time: 5:17 AM
 * (c) 2015
 */

$(document).ready(function () {

    $('#author').chosen({allow_single_deselect: true});

    $('.tablesorter').tablesorter({
        headers:{
            2:{
              sorter:false
            },
            6:{
                sorter:false
            }
        }
    });

    $('.calendar').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $('#preview').change(function(e){
        var reader = new FileReader();

        reader.onload = function (ev) {
            $('#imgPreview').attr('src',ev.target.result);
        }

        reader.readAsDataURL(this.files[0]);
    });

    $('.pic').click(function(ev){

        $(this).siblings().addClass('picbigshow').click(function (ev) {
            $(this).removeClass('picbigshow');
        });

    });

    $('#btnSearch').click(function (ev) {
        ev.preventDefault();
        var check = false;

        check = check || $('#author').val();
        check = check || $('#name').val();
        check = check || $('#date_from').val();
        check = check || $('#date_to').val();

        if(check){
            $('#frmSearch').submit();
        }
    });

    $('.delete-btn').click(function (ev) {
        ev.preventDefault();
        var elem = $(this);

        $.ajax({
            url:'/books/delete',
            data: {book_id: elem.data('book')},
            method:'post',
            dataType:'json',
            success: function (data) {
                data = JSON.parse(data);
                console.log(data);
                if(data.ok){
                    elem.closest('tr').remove();
                }
            }
        })
    });

    $('.view-btn').click(function (ev) {
        ev.preventDefault();
        var elem = $(this);

        $.ajax({
            url:'/books/view',
            data:{book_id:elem.data('book')},
            method:'post',
            dataType:'json',
            success: function (data) {
                data = JSON.parse(data);
                if(data.ok){
                    var jmodal = $('#bookViewModal');
                    jmodal.find('.modal-body').html(data.content);
                    jmodal.find('.modal-title').html(data.head);
                    jmodal.modal('show');
                }
            }
        })
    });
});