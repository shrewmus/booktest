<?php
/**
 * @file Image.php
 * @author shrewmus (shrewmus@yandex.ru contact@shrewmus.name)
 * Date: 11/25/15
 * Time: 7:28 AM
 * (c) 2015
 */

namespace app\models;


use yii\base\Model;
use yii\web\UploadedFile;

class Image extends Model
{
    /** @var  UploadedFile */
    public $file;
    private $_uploadedName;

    /**
     * @return mixed
     */
    public function getUploadedName()
    {
        return $this->_uploadedName;
    }

    public function rules()
    {
        return [
            [['file'],'file']
        ];
    }


    public function upload()
    {
        $dir = \Yii::getAlias('@app/web/upload');
        $name = uniqid('file_', false).'.'.$this->file->extension;
        $this->_uploadedName = \Yii::getAlias('@web/upload/').$name;
        return $this->file->saveAs($dir.'/'.$name);

    }
}