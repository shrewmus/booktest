<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $name
 * @property string $date_created
 * @property string $date_update
 * @property string $date
 * @property string $preview
 * @property integer $author_id
 *
 * @property Authors $author
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%books}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_created', 'date_update', 'date', 'preview', 'author_id'], 'required'],
            [['date_created', 'date_update', 'date'], 'safe'],
            [['author_id'], 'integer'],
            [['name', 'preview'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date_created' => 'Date Created',
            'date_update' => 'Date Update',
            'date' => 'Date',
            'preview' => 'Preview',
            'author_id' => 'Author ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }

    public function searchProvider($params){
        $query = Books::find();
        $query->andFilterWhere(['author_id'=>$params['author']]);
        if($params['date_from'] && $params['date_to']){
            $query->andFilterWhere(['between','date',$params['date_from'],$params['date_to']]);
        }else{
            $query->andFilterWhere(['>=','date',$params['date_from']]);
            $query->andFilterWhere(['<=','date',$params['date_to']]);
        }

        $query->andFilterWhere(['like','name',$params['name']]);
        return new ActiveDataProvider(['query'=>$query]);
    }
}
