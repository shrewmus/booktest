BOOKTEST Project
================

This is simple YII2 test project (based on basic yii application template) 

DEPLOY NOTE
-----------
Note that in this repository for some reason is absent file with db configuration config/db.php

in db.php you must place next code:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=booktest',
    'username' => '<db_user_name place here>',
    'password' => '<db_password place here>',
    'charset' => 'utf8',
];
```

RBAC or not
-----------
In current project access rules are very simple - only authorised users can view or create/edit books, so I decided to not use RBAC and AuthManager.
Using simple beforeAction is sufficient for purpose of current project.

Layout
------
In this project I use default layout, that comes with the framework - views/layouts/main.php
This layout already uses jQuery and Twitter Bootstrap

Demo user
---------
login: demo
pass: demo


for 
http://rgkgroup.com