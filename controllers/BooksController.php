<?php
/**
 * @file BooksController.php
 * @author shrewmus (shrewmus@yandex.ru contact@shrewmus.name)
 * Date: 11/25/15
 * Time: 4:30 AM
 * (c) 2015
 */

namespace app\controllers;


use app\models\Authors;
use app\models\Books;
use app\models\Image;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

class BooksController extends Controller
{

    public $layout = 'main';
    private $_formView = 'books_form';

    public function beforeAction($action)
    {
//        if (in_array($action->id, ['view', 'create', 'delete', 'edit'])) {
            if (\Yii::$app->user->isGuest) {
                $this->redirect('/login');
            }
//        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $books = Books::find()->all();
        $authors = Authors::find()->all();
        return $this->render('index', [
            'books' => $books,
            'authors' => $authors,
            'heads' => [
                'Id',
                'Название',
                'Превью',
                'Автор',
                'Дата выхода книги',
                'Дата добавления'
            ]
        ]);
    }

    public function actionView()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if(! \Yii::$app->user->isGuest){
            $bookid = \Yii::$app->request->post('book_id');
            /** @var Books $book */
            $book = Books::find()->where(['id'=>$bookid])->with('author')->one();
            $content = $this->renderPartial('books_view',['book'=>$book]);
            return json_encode(['ok' => 'ok', 'content' => urldecode($content),'head'=>$book->name]);
        }else{
            return json_encode(['err'=>['not authorised']]);
        }
    }

    public function actionCreate()
    {
        $authors = Authors::find()->all();
        $book = new Books();
        return $this->render($this->_formView, ['book' => $book,'authors'=>$authors,'type'=>'create']);
    }

    public function actionEdit($id)
    {
        $authors = Authors::find()->all();
        $book = Books::findOne(['id' => $id]);
        return $this->render($this->_formView, ['book' => $book,'authors'=>$authors,'type'=>'edit']);
    }

    public function actionSave()
    {
        $upload = false;
        //TODO: if already image file - delete file from fs
        if (isset($_FILES)) {
            $upload = true;
            $model = new Image();
            $model->file = UploadedFile::getInstanceByName('preview');
            $model->upload();
        }

        $formType = \Yii::$app->request->post('type');
        if ($formType === 'create') {
            /** @var Books $book */
            $book = new Books();
        } else {
            $book = Books::findOne(['id' => \Yii::$app->request->post('book_id')]);
        }

        $currDate = Date('Y-m-d H:i:s');
        $book->name = \Yii::$app->request->post('name');
        $book->date = \Yii::$app->request->post('date');
        $book->author_id = \Yii::$app->request->post('author');
        $book->date_update = $currDate;
        $book->preview = 'No image';
        if ($formType === 'create') {
            $book->date_created = $currDate;
        }
        if($upload){
            if($model->file){
                $book->preview = $model->getUploadedName();
            }
        }
        $res = $book->save();
        $this->redirect('/');


    }

    public function actionDelete()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $id = \Yii::$app->request->post('book_id');
        /** @var Books $book */
        $book = Books::findOne(['id'=>$id]);
        if($book->preview){
            unlink($book->preview);
        }
        $res = $book->delete();
        return json_encode(['ok' => $res]);
    }

    public function actionSearch()
    {
        $searchParams = [
            'author' => \Yii::$app->request->post('author'),
            'date_from' => \Yii::$app->request->post('date_from'),
            'date_to' => \Yii::$app->request->post('date_to'),
            'name' => \Yii::$app->request->post('name')
        ];
        /** @var ActiveDataProvider $search */
        $search = (new Books())->searchProvider($searchParams);
        $books = $search->getModels();
        $authors = Authors::find()->all();
        return $this->render('index', [
            'books' => $books,
            'authors' => $authors,
            'search'=>$searchParams,
            'heads' => [
                'Id',
                'Название',
                'Превью',
                'Автор',
                'Дата выхода книги',
                'Дата добавления'
            ]
        ]);
    }


}